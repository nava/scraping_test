from pymongo import MongoClient
from selenium import webdriver
from pyvirtualdisplay import Display

import requests


def load_webdriver(name, headless=False):
    '''
    @param name: this is the name of the driver object from selenium webdriver
    The possible values are chrome, phantomjs, firefox, firefox profile.
    You can extend this for your reasons
    '''
    driver = None
    display = None

    if headless:
        display = Display(visible=False, size=(1280,960))
        display.start()

    if name.lower() == 'chrome':
        driver = webdriver.Chrome()
    elif name.lower() == 'firefox':
        driver = webdriver.Firefox()
    elif name.lower() == 'firefox_profile':
        driver = webdriver.FirefoxProfile()
    elif name.lower() == 'phantomjs':
        dcap = dict(webdriver.DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = ( "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit" )
        driver = webdriver.PhantomJS(desired_capabilities=dcap)
        driver.set_window_size(1151, 629)

    return driver, display

