import multiprocessing
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time

from utils import load_webdriver


def getNewOrderIds(driver):
    WebDriverWait(driver, 15).until(EC.presence_of_all_elements_located(driver.find_elements_by_class_name('order-row')))
    order_ids = []
    rows = driver.find_elements_by_class_name('order-row')
    print rows
    for row in rows:
        print row
        if 'Unshipped' in row.get_attribute('innerHTML'):
            order_ids.append(row.find_element_by_xpath('.//td[3]/a/strong').get_attribute('innerHTML'))

    while True:
        try:
            driver.find_element_by_link_text('Next').click()
            time.sleep(1)
        except:
            break

        rows = driver.find_elements_by_class_name('order-row')
        for row in rows:
            print row
            if 'Unshipped' in row.get_attribute('innerHTML'):
                order_ids.append(row.find_element_by_xpath('.//td[3]/a/strong').get_attribute('innerHTML'))

    print order_ids


def amazon_scraping(proc, amazon_username, amazon_password):
    driver, display = load_webdriver('phantomjs', headless=False)
    driver.get('https://sellercentral.amazon.in/gp/homepage.html')
    driver.find_element_by_id('username').send_keys(amazon_username)
    driver.find_element_by_id('password').send_keys(amazon_password)
    driver.find_element_by_id('sign-in-button').click()

    time.sleep(5)


    with open('%s.html' % proc,'wb') as f:
        f.write(driver.page_source.encode('utf-8'))

    try:
        getNewOrderIds(driver)
    except Exception as e:
        print str(e)
        with open('AmazonError_%s.html' % proc, 'wb') as fob:
            fob.write(driver.page_source.encode('utf-8'))


if __name__ == '__main__':
    jobs = []
    for name in range(3):
        p = multiprocessing.Process(target=amazon_scraping, args=(name, "bboybodywear@gmail.com", "pinkbeer12#"),
                                    name=name)
        jobs.append(p)
        p.start()

    for j in jobs:
        j.join()
        print '%s.exitcode = %s' % (j.name, j.exitcode)
